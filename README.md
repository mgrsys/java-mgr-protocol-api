##Project goal

The project includes the implementation of magora api protocol.   

##How to build

In order to compile this sources you need to install:

1. Maven 3 >
2. Git 2.7 > 
3. Java 1.8.100 >


```
cd /reposes
git clone https://bitbucket.org/mgrsys/java-mgr-protocol-api
cd java-mgr-protocol-api
```

Build source code

```
mvn clean package
```

Upload artifacts to nexus

```
mvn clean source:jar deploy
```

Please note, that you should have the appropriate settings into your settings.xml 

```xml
<servers>
    <server>
        <id>magora-repository</id>
        <username>deployment</username>
        <password>password</password>
    </server>
</servers>
```

