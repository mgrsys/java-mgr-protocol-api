package com.magorasystems.protocolapi.spring;

import com.magorasystems.protocolapi.response.ErrorResponses;
import com.magorasystems.protocolapi.response.ResponseCodes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(GlobalExceptionHandlerController.ERROR_PATH)
public class GlobalExceptionHandlerController implements ErrorController {

    final static String ERROR_PATH = "/error";

    @RequestMapping
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseEntity error() {
        return new ResponseEntity<>(
                ErrorResponses.singleError(
                        ResponseCodes.ERROR_COMMON_CODE_RESOURCE_NOT_FOUND,
                        "Requested URL doesn't exist",
                        null), HttpStatus.NOT_FOUND
        );
    }

    @Override
    public String getErrorPath() {
        return GlobalExceptionHandlerController.ERROR_PATH;
    }
}