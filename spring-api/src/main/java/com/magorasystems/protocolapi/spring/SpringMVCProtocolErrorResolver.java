package com.magorasystems.protocolapi.spring;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.magorasystems.protocolapi.exception.CodeAwareTrouble;
import com.magorasystems.protocolapi.exception.NonAuthorizedTrouble;
import com.magorasystems.protocolapi.exception.ResourceForbiddenTrouble;
import com.magorasystems.protocolapi.response.*;
import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.magorasystems.protocolapi.response.ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_INVALID;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class SpringMVCProtocolErrorResolver extends AbstractHandlerExceptionResolver {

    private static final Logger logger = LoggerFactory.getLogger(SpringMVCProtocolErrorResolver.class);

    protected final View view;

    private boolean stringifyHttpCode = true;

    private Map<Integer, String> httpCode2stringCode;

    private final RequestStringPresenter requestStringPresenter = new RequestStringPresenter();

    // we use the string class name to avoid spring security dependencies.
    private static final Set<String> SECURITY_ISSUES_CLASSES = Collections.singleton(
            "org.springframework.security.access.AccessDeniedException" // @Secured annotation throws it.
    );

    public SpringMVCProtocolErrorResolver() {
        MappingJackson2JsonView view = new MappingJackson2JsonView();
        view.setExtractValueFromSingleKeyModel(true);
        this.view = view;
        initHttpCodes();
    }

    public SpringMVCProtocolErrorResolver(View view) {
        this.view = view;
        initHttpCodes();
    }

    @Override
    protected ModelAndView doResolveException
            (HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        ErrorResponse errorResponse = null;
        int status = HttpCodes.HTTP_ERROR_INTERNAL_ERROR;

        if (ex instanceof CodeAwareTrouble) {
            CodeAwareTrouble cex = (CodeAwareTrouble) ex;

            errorResponse = ErrorResponses.singleError(cex.getCode(), cex.getMessage(), cex.getField());

            status = HttpCodes.HTTP_UNPROCESSABLE_ENTITY;

            if (ex instanceof NonAuthorizedTrouble) {
                status = HttpCodes.HTTP_SECURITY_UNAUTHORIZED;
            } else if (ex instanceof ResourceForbiddenTrouble) {
                status = HttpCodes.HTTP_SECURITY_FORBIDDEN;
            }

        } else if (ex instanceof ConstraintViolationException) {
            ConstraintViolationException exx = (ConstraintViolationException) ex;

            List<ResponseErrorField> errors = exx.getConstraintViolations()
                    .stream()
                    .map(item -> {

                        Optional<String> fieldOpt = Stream.of(item.getPropertyPath().toString().split("\\."))
                                .skip(2)
                                .reduce((a, b) -> a.concat(".").concat(b));

                        final String field = fieldOpt.orElse(null);

                        return new ResponseErrorField(JSR303ToAPIConverter.getCode(
                                item.getConstraintDescriptor().getAnnotation().annotationType(),
                                ERROR_COMMON_CODE_FORM_FIELD_INVALID),
                                item.getMessage(), field);
                    })
                    .collect(Collectors.toList());

            errorResponse = new ErrorResponse(ResponseCodes.ERROR_COMMON_UNPROCESSABLE_ENTITY, errors, "Unprocessable Entity");
            status = HttpCodes.HTTP_UNPROCESSABLE_ENTITY;
        } else if (ex instanceof HttpMessageConversionException) {

            Throwable cause = ex.getCause();

            // jackson error handling
            if (cause instanceof JsonMappingException) {
                JsonMappingException jme = (JsonMappingException) cause;

                String fieldName = jme.getPath().isEmpty() ? null : jme.getPath().get(0).getFieldName();

                if (jme.getCause() instanceof CodeAwareTrouble) {
                    CodeAwareTrouble cex = (CodeAwareTrouble) jme.getCause();
                    errorResponse = ErrorResponses.singleError(cex.getCode(), cex.getMessage(), Optional.ofNullable(cex.getField()).orElse(fieldName));
                } else {
                    errorResponse = new ErrorResponse(ResponseCodes.ERROR_COMMON_UNPROCESSABLE_ENTITY,
                            Collections.singletonList(new ResponseErrorField(ERROR_COMMON_CODE_FORM_FIELD_INVALID,
                                    "Value can't be recognized", fieldName)
                            ), "Unprocessable Entity");
                }
                status = HttpCodes.HTTP_UNPROCESSABLE_ENTITY;
            } else if (cause != null) {
                errorResponse = ErrorResponses.singleError(ResponseCodes.ERROR_COMMON_BAD_PARAMETERS, "The request can not be read", null);
                status = HttpCodes.HTTP_ERROR_BAD_REQUEST;
            }
        } else if (ex instanceof TypeMismatchException) {
            errorResponse = ErrorResponses.singleError(ResponseCodes.ERROR_COMMON_UNPROCESSABLE_ENTITY, "Unprocessable Entity, check types and formats of fields", null);
            status = HttpCodes.HTTP_UNPROCESSABLE_ENTITY;
        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            errorResponse = ErrorResponses.singleError(ResponseCodes.ERROR_COMMON_CODE_BSN_CONFLICT, "Request method not supported", null);
            status = HttpCodes.HTTP_ERROR_CONFLICT;
        } else if (ex instanceof BindException) {
            List<ResponseErrorField> errors = ((BindException) ex).getAllErrors()
                    .stream()
                    .map(item -> new ResponseErrorField(ERROR_COMMON_CODE_FORM_FIELD_INVALID,
                            "Value can't be recognized",
                            item instanceof FieldError ? ((FieldError) item).getField() : item.getObjectName()
                    ))
                    .collect(Collectors.toList());
            errorResponse = new ErrorResponse(ResponseCodes.ERROR_COMMON_UNPROCESSABLE_ENTITY, errors, "Unprocessable Entity");
            status = HttpCodes.HTTP_UNPROCESSABLE_ENTITY;
        } else if (SECURITY_ISSUES_CLASSES.contains(ex.getClass().getName())) {
            errorResponse = ErrorResponses.singleError(
                    AuthResponseCodes.COMMON_FORBIDDEN_ERROR,
                    "Resource forbidden",
                    null);
            status = HttpCodes.HTTP_SECURITY_FORBIDDEN;
        }

        if (errorResponse == null) {
            long problemCode = System.currentTimeMillis();
            errorResponse = ErrorResponses.singleError(
                    ResponseCodes.ERROR_COMMON_CODE_UNKNOWN_ERROR,
                    String.format("Happens unexpected error. Pass the code [%s] to system administrator", problemCode),
                    null);
            logger.error("Unexpected error: [" + problemCode + "]", ex);
        }

        if (stringifyHttpCode) {
            String statusString = httpCode2stringCode.get(status);
            if (statusString == null) statusString = ResponseCodes.ERROR_COMMON_CODE_UNKNOWN_ERROR;
            errorResponse = new ErrorResponse(statusString, errorResponse.getErrors(), errorResponse.getMessage());
        }

        try {
            if (logger.isErrorEnabled() && status == HttpCodes.HTTP_ERROR_INTERNAL_ERROR) {
                logger.error(requestStringPresenter.createMessage(request));
            } else if (logger.isDebugEnabled()
                    && status != HttpCodes.HTTP_UNPROCESSABLE_ENTITY
                    && status != HttpCodes.HTTP_SECURITY_FORBIDDEN
                    && status != HttpCodes.HTTP_SECURITY_UNAUTHORIZED
                    ) {
                logger.debug(requestStringPresenter.createMessage(request));
            } else if (logger.isTraceEnabled()) {
                logger.trace(requestStringPresenter.createMessage(request));
            }

        } catch (Exception e) {
            logger.warn("Some error during stringify of http request", e);
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(view);
        modelAndView.addObject(errorResponse);
        response.setStatus(status);

        return modelAndView;

    }

    public boolean isStringifyHttpCode() {
        return stringifyHttpCode;
    }

    public void setStringifyHttpCode(boolean stringifyHttpCode) {
        this.stringifyHttpCode = stringifyHttpCode;
    }

    private void initHttpCodes() {
        httpCode2stringCode = new HashMap<>();

        httpCode2stringCode.put(HttpCodes.HTTP_ERROR_CONFLICT, ResponseCodes.ERROR_COMMON_CODE_BSN_CONFLICT);
        httpCode2stringCode.put(HttpCodes.HTTP_ERROR_BAD_REQUEST, ResponseCodes.ERROR_COMMON_BAD_PARAMETERS);
        httpCode2stringCode.put(HttpCodes.HTTP_ERROR_INTERNAL_ERROR, ResponseCodes.ERROR_COMMON_CODE_UNKNOWN_ERROR);
        httpCode2stringCode.put(HttpCodes.HTTP_ERROR_NOT_FOUND, ResponseCodes.ERROR_COMMON_CODE_RESOURCE_NOT_FOUND);
        httpCode2stringCode.put(HttpCodes.HTTP_SECURITY_UNAUTHORIZED, ResponseCodes.ERROR_COMMON_SECURITY_CODE);
        httpCode2stringCode.put(HttpCodes.HTTP_SECURITY_FORBIDDEN, ResponseCodes.ERROR_COMMON_FORBIDDEN_CODE);
        httpCode2stringCode.put(HttpCodes.HTTP_UNPROCESSABLE_ENTITY, ResponseCodes.ERROR_COMMON_UNPROCESSABLE_ENTITY);
    }
}
