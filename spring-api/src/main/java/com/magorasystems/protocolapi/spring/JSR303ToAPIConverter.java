package com.magorasystems.protocolapi.spring;

import com.magorasystems.protocolapi.response.ResponseCodes;
import org.hibernate.validator.constraints.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class JSR303ToAPIConverter {

    private static final Map<Class<? extends Annotation>, String> validation2code;

    static {
        Map<Class<? extends Annotation>, String> init = new HashMap<>();
        init.put(Email.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_EMAIL);
        init.put(NotEmpty.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_NOT_BLANK);
        init.put(NotBlank.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_NOT_BLANK);
        init.put(NotNull.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_NOT_BLANK);
        init.put(Max.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_SIZE_MAX);
        init.put(Min.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_SIZE_MIN);
        init.put(Length.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_INVALID_LENGTH);
        init.put(Size.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_INVALID_SIZE);
        init.put(URL.class, ResponseCodes.ERROR_COMMON_CODE_FORM_FIELD_INVALID_URL);

        validation2code = Collections.unmodifiableMap(init);

    }

    public static String getCode(Class<? extends Annotation> clazz, String defaultCode) {
        return validation2code.containsKey(clazz) ? validation2code.get(clazz) : defaultCode;
    }

}
