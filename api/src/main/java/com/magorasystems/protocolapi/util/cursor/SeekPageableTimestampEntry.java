package com.magorasystems.protocolapi.util.cursor;

/**
 * Created by kemenov on 07.11.2016.
 */
public interface SeekPageableTimestampEntry<T> {

    T getCursor();

    long getTimestamp();

}
