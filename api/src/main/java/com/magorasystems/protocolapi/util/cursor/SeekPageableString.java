package com.magorasystems.protocolapi.util.cursor;



/**
 * Created by kemenov on 19.10.2016.
 */
public class SeekPageableString implements SeekPageable<String> {

    private final String cursor;
    private final Direction direction;
    private final int pageSize;
    private final Long timestamp;

    public SeekPageableString(String cursor, Direction direction, int pageSize, Long timestamp) {
        this.cursor = cursor;
        this.direction = direction;
        this.pageSize = pageSize;
        this.timestamp = timestamp;
    }


    @Override
    public String getCursor() {
        return cursor;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    public int getPageSize() {
        return pageSize;
    }
}
