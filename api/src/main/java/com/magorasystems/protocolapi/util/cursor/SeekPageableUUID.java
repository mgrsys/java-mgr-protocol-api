package com.magorasystems.protocolapi.util.cursor;


import java.util.UUID;

/**
 * Created by kemenov on 19.10.2016.
 */
public class SeekPageableUUID implements SeekPageable<UUID> {

    private final UUID cursor;
    private final Direction direction;
    private final int pageSize;
    private final Long timestamp;

    public SeekPageableUUID(UUID cursor, Direction direction, int pageSize, Long timestamp) {
        this.cursor = cursor;
        this.direction = direction;
        this.pageSize = pageSize;
        this.timestamp = timestamp;
    }


    @Override
    public UUID getCursor() {
        return cursor;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    public int getPageSize() {
        return pageSize;
    }
}
