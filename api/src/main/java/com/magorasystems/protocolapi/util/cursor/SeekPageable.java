package com.magorasystems.protocolapi.util.cursor;

/**
 * Created by kemenov on 07.10.2016.
 */
public interface SeekPageable<T> {

    enum Direction {
        TOP("T"),
        BOTTOM("B");

        private final String symbol;

        Direction(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }

        public static Direction fromSymbol(String s) {
            return "T".equalsIgnoreCase(s) ? TOP : BOTTOM;
        }

    }

    int getPageSize();

    T getCursor();

    Direction getDirection();

    Long getTimestamp();

}
