package com.magorasystems.protocolapi.util.cursor;

import com.magorasystems.protocolapi.exception.impl.InvalidCursorException;
import com.magorasystems.protocolapi.request.Cursorable;
import com.magorasystems.protocolapi.request.PageRequest;
import com.magorasystems.protocolapi.request.Pageable;
import com.magorasystems.protocolapi.response.CursorData;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public class PageCursors {

    private final static int DEFAULT_SIZE = PageRequest.PAGE_SIZE;
    private final static int FIRST_PAGE_INDEX = 0;

    private final static Pageable FIRST_PAGE = new PageRequest(FIRST_PAGE_INDEX, DEFAULT_SIZE);
    private static final String UTF_8 = "UTF-8";

    public static Pageable buildPageRequest(Cursorable cursorable) {
        String origCursor = cursorable.getCursor();
        if (origCursor != null && !origCursor.isEmpty()) {
            final int cursorId;
            try {
                cursorId = Integer.parseInt(new String(DatatypeConverter.parseBase64Binary(origCursor), UTF_8));
            } catch (Throwable throwable) {
                throw new InvalidCursorException();
            }
            return new PageRequest(
                    cursorId,
                    cursorable.getPageSize()
            );
        }
        if (cursorable.getPageSize() == DEFAULT_SIZE) {
            return FIRST_PAGE;


        }
        return new PageRequest(FIRST_PAGE_INDEX, cursorable.getPageSize());
    }


    public static <T> CursorData<T> buildPageCursor(List<T> items, Pageable request) {
        return new CursorData<T>(
                items,
                encode2String((items.size() < request.getPageSize()) ? null : String.valueOf(request.getPage() + 1)),
                encode2String(request.getPage() == FIRST_PAGE_INDEX ?
                        null :
                        String.valueOf(request.getPage() - 1)
                )
        );
    }

    private static String encode2String(String or) {
        if (or == null) {
            return null;
        }
        try {
            return DatatypeConverter.printBase64Binary(or.getBytes(UTF_8));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }


}
