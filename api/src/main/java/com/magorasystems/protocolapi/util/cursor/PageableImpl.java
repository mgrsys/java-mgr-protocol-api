package com.magorasystems.protocolapi.util.cursor;


import com.magorasystems.protocolapi.request.Pageable;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 *
 * @Deprecated use com.magorasystems.protocolapi.request.PageRequest
 */
@Deprecated
public class PageableImpl implements Pageable {

    private final int page;
    private final int pageSize;

    public PageableImpl(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }
}
