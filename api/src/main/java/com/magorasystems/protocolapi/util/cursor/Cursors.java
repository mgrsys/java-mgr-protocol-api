package com.magorasystems.protocolapi.util.cursor;


import com.magorasystems.protocolapi.request.Cursorable;
import com.magorasystems.protocolapi.request.Pageable;
import com.magorasystems.protocolapi.response.CursorData;

import java.util.List;
import java.util.UUID;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 *
 * @Deprecated use com.magorasystems.protocolapi.util.cursor.PageCursors
 */
@Deprecated
public class Cursors {

    private final static int DEFAULT_SIZE = 100;
    private final static int FIRST_PAGE_INDEX = 0;

    public final static Pageable FIRST_PAGE = new PageableImpl(FIRST_PAGE_INDEX, DEFAULT_SIZE);

    private static final String SEEK_TIMESTAMP_CURSOR_PATTERN = "%s|%s|%s";


    public static Pageable buildPageRequest(Cursorable cursorable) {
        String cursor = cursorable.getCursor();
        if (cursor != null && !cursor.isEmpty()) {
            return new PageableImpl(
                    Integer.parseInt(cursor),
                    cursorable.getPageSize()
            );
        }
        if (cursorable.getPageSize() == DEFAULT_SIZE) {
            return FIRST_PAGE;
        }
        return new PageableImpl(0, cursorable.getPageSize());
    }

    public static <T> SeekPageable<T> buildSeekPageRequest(Cursorable cursorable, Class<T> cursorType) {
        if (!String.class.isAssignableFrom(cursorType) && !UUID.class.isAssignableFrom(cursorType)) {
            throw new IllegalStateException("Cursor type isn't supported");
        }
        String cursor = cursorable.getCursor();
        if (cursor != null && !cursor.isEmpty()) {
            String[] split = cursor.split("\\|");
            SeekPageable.Direction dir;
            UUID id = null;
            Long timestamp = null;
            if (split.length > 0) {
                dir = SeekPageable.Direction.fromSymbol(split[0]);
                if (split.length > 1) {
                    id = UUID.fromString(split[1]);
                    if (split.length > 2) {
                        timestamp = Long.parseLong(split[2]);
                    }
                }
            } else {
                throw new IllegalStateException("Illegal page request");
            }

            SeekPageableUUID seekPageableUUID = new SeekPageableUUID(
                    id,
                    dir,
                    cursorable.getPageSize(),
                    timestamp);

            return (SeekPageable<T>) seekPageableUUID;
        }
        return (SeekPageable<T>) new SeekPageableUUID(null, SeekPageable.Direction.BOTTOM, cursorable.getPageSize(), null);
    }

    public static <T> CursorData<T> buildPageCursor(List<T> items, Pageable request) {
        return new CursorData<T>(
                items,
                items.isEmpty() ? null : String.valueOf(request.getPage() + 1),
                request.getPage() == 0 ?
                        null :
                        String.valueOf(request.getPage() - 1)
        );
    }

    public static <T extends SeekPageableTimestampEntry> String buildCursor(T item, SeekPageable.Direction direction) {
        return String.format(SEEK_TIMESTAMP_CURSOR_PATTERN, direction.getSymbol(), item.getCursor(), item.getTimestamp());
    }

    public static <T extends SeekPageableTimestampEntry> CursorData<T> buildSeekPageCursor(List<T> items, SeekPageable request) {

        String privCursor = null;
        String nextCursor = null;
        SeekPageable.Direction direction = request.getDirection();

        if (!items.isEmpty()) {
            if (SeekPageable.Direction.BOTTOM == direction || request.getCursor() != null) {
                privCursor = buildCursor(items.get(0), SeekPageable.Direction.TOP);
            }

            if (SeekPageable.Direction.TOP == direction || request.getCursor() != null) {
                nextCursor = buildCursor(items.get(items.size() - 1), SeekPageable.Direction.BOTTOM);
            }
        } else {
            if (request.getCursor() != null) {
                if (direction == SeekPageable.Direction.BOTTOM) {
                    privCursor = SeekPageable.Direction.TOP.getSymbol();
                } else {
                    nextCursor = SeekPageable.Direction.BOTTOM.getSymbol();
                }
            }
        }


        return new CursorData<T>(
                items,
                nextCursor,
                privCursor
        );
    }

}
