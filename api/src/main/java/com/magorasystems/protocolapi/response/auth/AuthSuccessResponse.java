package com.magorasystems.protocolapi.response.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.magorasystems.protocolapi.response.SuccessResponse;

/**
 * Class represents auth response.
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class AuthSuccessResponse<T extends AuthResponseData> extends SuccessResponse<T> {

    @JsonCreator
    public AuthSuccessResponse(@JsonProperty("code") String code, @JsonProperty("data") T data) {
        super(code, data);
    }



}
