package com.magorasystems.protocolapi.response.auth;

import java.io.Serializable;

/**
 * Base class for custom auth details. Contains required fields.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public abstract class AbstractAuthInfo<I extends Serializable> implements AuthInfo<I> {

    /* User display name */
    private final String displayName;

    /* User ID */
    private final I userId;

    public AbstractAuthInfo(String displayName, I userId) {
        this.displayName = displayName;
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public I getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "AbstractAuthInfo{" +
                "displayName='" + displayName + '\'' +
                ", userId=" + userId +
                '}';
    }
}
