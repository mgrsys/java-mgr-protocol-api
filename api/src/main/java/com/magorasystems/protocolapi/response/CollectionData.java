package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by kemenov on 25.04.2017.
 */
public class CollectionData<T> {

    private final List<T> items;

    @JsonCreator
    public CollectionData(@JsonProperty("items") List<T> items) {
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }
}
