package com.magorasystems.protocolapi.response;

/**
 * Base model for all responses
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public abstract class Response {

    /* response code  */
    private final String code;

    public Response(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code='" + code + '\'' +
                '}';
    }
}
