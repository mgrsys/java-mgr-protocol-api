package com.magorasystems.protocolapi.response.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Class represents auth response data.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class AuthResponseData<T extends AuthInfo> {

    /* Access Token */
    private final String accessToken;

    /* Access Token Expire */
    private final Date accessTokenExpire;

    /* Refresh Token */
    private final String refreshToken;

    /* Auth Info */
    private final T authInfo;

    @JsonCreator
    public AuthResponseData(@JsonProperty("accessToken") String accessToken,
                            @JsonProperty("accessTokenExpire") long accessTokenExpire,
                            @JsonProperty("refreshToken") String refreshToken,
                            @JsonProperty("authInfo") T authInfo) {
        this.accessToken = accessToken;
        this.accessTokenExpire = new Date(accessTokenExpire);
        this.refreshToken = refreshToken;
        this.authInfo = authInfo;
    }

    @JsonCreator
    public AuthResponseData(@JsonProperty("accessToken") String accessToken,
                            @JsonProperty("accessTokenExpire") Date accessTokenExpire,
                            @JsonProperty("refreshToken") String refreshToken,
                            @JsonProperty("authInfo") T authInfo) {
        this.accessToken = accessToken;
        this.accessTokenExpire = accessTokenExpire;
        this.refreshToken = refreshToken;
        this.authInfo = authInfo;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public Date getAccessTokenExpire() {
        return accessTokenExpire;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public T getAuthInfo() {
        return authInfo;
    }

    @Override
    public String toString() {
        return "AuthResponseData{" +
                "accessToken='" + accessToken + '\'' +
                ", accessTokenExpire='" + accessTokenExpire + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", authInfo=" + authInfo +
                '}';
    }
}
