package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.List;

/**
 * Created by kemenov on 25.04.2017.
 */
public class CursorData<T> extends CollectionData<T> {

    private final String nextCursor;
    private final String prevCursor;

    @JsonCreator
    public CursorData(@JsonProperty("items") List<T> items,
                      @JsonProperty("nextCursor") String nextCursor,
                      @JsonProperty("prevCursor") String prevCursor) {
        super(items);
        this.nextCursor = nextCursor;
        this.prevCursor = prevCursor;
    }

    public String getNextCursor() {
        return nextCursor;
    }

    public String getPrevCursor() {
        return prevCursor;
    }

    @Override
    public String toString() {
        return "CursorData{" +
                "nextCursor='" + nextCursor + '\'' +
                ", prevCursor='" + prevCursor + '\'' +
                '}';
    }
}
