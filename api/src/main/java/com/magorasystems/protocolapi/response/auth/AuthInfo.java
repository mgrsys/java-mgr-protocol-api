package com.magorasystems.protocolapi.response.auth;

import java.io.Serializable;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public interface AuthInfo<I extends Serializable> {

    /* Gets display name */
    String getDisplayName();

    /* Gets user id */
    I getUserId();

}
