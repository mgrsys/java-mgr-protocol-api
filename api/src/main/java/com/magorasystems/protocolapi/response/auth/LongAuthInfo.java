package com.magorasystems.protocolapi.response.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class provides simple auth info with Numeric ID.
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class LongAuthInfo extends AbstractAuthInfo<Long> {

    @JsonCreator
    public LongAuthInfo(@JsonProperty("displayName") String displayName,
                        @JsonProperty("userId") Long userId) {
        super(displayName, userId);
    }


}
