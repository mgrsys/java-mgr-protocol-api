package com.magorasystems.protocolapi.response.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * Class provides simple auth info with UUID.
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2016.
 */
public class UUIDAuthInfo extends AbstractAuthInfo<UUID> {

    @JsonCreator
    public UUIDAuthInfo(@JsonProperty("displayName") String displayName,
                        @JsonProperty("userId") UUID uuid) {
        super(displayName, uuid);
    }
}
