package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class represents thee error field in error response
 *
 * @author : Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class ResponseErrorField {

    /* Error Code */
    private final String code;

    /* Error Message */
    private final String message;

    /* Error Field Name */
    private final String field;

    @JsonCreator
    public ResponseErrorField(@JsonProperty("code") String code,
                              @JsonProperty("message") String message,
                              @JsonProperty("field") String field) {
        this.code = code;
        this.message = message;
        this.field = field;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getField() {
        return field;
    }
}
