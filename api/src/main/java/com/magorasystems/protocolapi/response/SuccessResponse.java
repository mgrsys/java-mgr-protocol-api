package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Base success response.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class SuccessResponse<T> extends Response {

    /* data */
    private final T data;

    @JsonCreator
    public SuccessResponse(@JsonProperty("data") T data) {
        super(ResponseCodes.SUCCESS_CODE);
        this.data = data;
    }

    public SuccessResponse(String code, T data) {
        super(code);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "SuccessResponse{" +
                "data=" + data +
                "} " + super.toString();
    }
}
