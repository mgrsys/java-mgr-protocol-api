package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class SuccessEmptyResponse extends SuccessResponse<Void> {

    private static SuccessEmptyResponse instance;

    public SuccessEmptyResponse() {
        super(ResponseCodes.SUCCESS_CODE, null);
    }

    @JsonCreator
    public SuccessEmptyResponse(@JsonProperty("code") String code) {
        super(code, null);
    }

    public static SuccessEmptyResponse getInstance() {
        if (instance == null) {
            synchronized (SuccessEmptyResponse.class) {
                if (instance == null) {
                    instance = new SuccessEmptyResponse();
                }
            }
        }
        return instance;
    }

}
