package com.magorasystems.protocolapi.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * Base success response.
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class ErrorResponse extends Response {

    /* List of errors */
    private final List<ResponseErrorField> errors;

    /* Error message */
    private final String message;

    @JsonCreator
    public ErrorResponse(@JsonProperty("code") String code,
                         @JsonProperty("errors") List<ResponseErrorField> errors,
                         @JsonProperty("message") String message) {
        super(code);
        this.errors = errors;
        this.message = message;
    }

    /**
     * Returns list of errors. Can't be NULL.
     *
     * @return list of errors
     */
    public List<ResponseErrorField> getErrors() {
        if (this.errors == null) {
            return Collections.emptyList();
        }
        return errors;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errors=" + errors +
                ", message='" + message + '\'' +
                "} " + super.toString();
    }


}
