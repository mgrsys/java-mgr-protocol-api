package com.magorasystems.protocolapi.response.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class provides simple auth info with String ID.
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class StringAuthInfo extends AbstractAuthInfo<String> {

    @JsonCreator
    public StringAuthInfo(@JsonProperty("displayName") String displayName,
                          @JsonProperty("userId") String userId) {
        super(displayName, userId);
    }
}
