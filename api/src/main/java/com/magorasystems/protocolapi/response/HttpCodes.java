package com.magorasystems.protocolapi.response;

/**
 * Declares HTTP codes
 *
 * @author  Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public final class HttpCodes {

    private HttpCodes() {

    }

    public static final int HTTP_SUCCESS_OK = 200;
    public static final int HTTP_SUCCESS_CREATED = 201;

    public static final int HTTP_ERROR_NOT_FOUND = 404;
    public static final int HTTP_ERROR_BAD_REQUEST = 400;
    public static final int HTTP_UNPROCESSABLE_ENTITY = 422;
    public static final int HTTP_ERROR_CONFLICT = 409;
    public static final int HTTP_ERROR_INTERNAL_ERROR = 500;

    public static final int HTTP_SECURITY_UNAUTHORIZED = 401;
    public static final int HTTP_SECURITY_FORBIDDEN = 403;

}
