package com.magorasystems.protocolapi.response;

/**
 * Class declares common error codes
 *
 * @author Fyodor Kemenov
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public final class ResponseCodes {

    private ResponseCodes() {
    }

    public static final String SUCCESS_CODE = "success";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_INVALID = "common.field_invalid";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_NOT_BLANK = "common.field_not_blank";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_SIZE_MAX = "common.field_size_max";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_SIZE_MIN = "common.field_size_min";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_INVALID_LENGTH = "common.field_invalid_length";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_INVALID_SIZE = "common.field_invalid_size";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_INVALID_URL = "common.field_invalid_url";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_NOT_VALID_CHARS = "common.field_not_valid_chars";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_MAX = "common.field_max";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_MIN = "common.field_min";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_FUTURE = "common.field_future";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_PAST = "common.field_past";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_EMAIL = "common.field_email";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_CARD_NUMBER = "common.field_card_number";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_PHONE = "common.field_phone";
    public static final String ERROR_COMMON_CODE_FORM_FIELD_DUPLICATE = "common.field_duplicate";
    public static final String ERROR_COMMON_CODE_INVALID_CURSOR = "common.invalid_cursor";


    public static final String ERROR_COMMON_CODE_UNKNOWN_ERROR = "internal_error";
    public static final String ERROR_COMMON_BAD_PARAMETERS = "bad_parameters";
    public static final String ERROR_COMMON_UNPROCESSABLE_ENTITY = "unprocessable_entity";

    public static final String ERROR_COMMON_CODE_RESOURCE_NOT_FOUND = "not_found";
    public static final String ERROR_COMMON_CODE_BSN_CONFLICT = "business_conflict";

    public static final String ERROR_COMMON_SECURITY_CODE = "security_error";
    public static final String ERROR_COMMON_FORBIDDEN_CODE = "permission_error";

    public static final String ERROR_GENERIC_RESOURCE_ALREADY_EXISTS_CODE = "common.resource_already_exists";


}
