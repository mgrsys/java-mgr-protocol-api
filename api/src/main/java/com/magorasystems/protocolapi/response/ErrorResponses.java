package com.magorasystems.protocolapi.response;

import java.util.Collections;

/**
 * Factory class for Error Responses
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class ErrorResponses {

    /**/
    private ErrorResponses() {
    }

    public static ErrorResponse singleError(String code, String message, String field) {
        return new ErrorResponse(code, Collections.singletonList(new ResponseErrorField(code, message, field)), message);
    }

    public static ErrorResponse singleError(String globalCode, String code, String message, String field) {
        return new ErrorResponse(globalCode, Collections.singletonList(new ResponseErrorField(code, message, field)), message);
    }

}
