package com.magorasystems.protocolapi.response.auth;

/**
 * List of codes for auth module.
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class AuthResponseCodes {

    private AuthResponseCodes() {

    }

    public static final String AUTH_PREFIX = "sec.";
    public static final String INVALID_AUTH_DATA = "sec.invalid_auth_data";
    public static final String LOGIN_SHOULD_BE_CONFIRMED = "sec.login_should_be_confirmed";
    public static final String REFRESH_TOKEN_INVALID_ERROR = "sec.refresh_token_invalid";
    public static final String ACCESS_TOKEN_INVALID_ERROR = "sec.access_token_invalid";
    public static final String ACCESS_TOKEN_EXPIRED_ERROR = "sec.access_token_expired";
    public static final String REFRESH_TOKEN_EXPIRED_ERROR = "sec.refresh_token_expired";
    public static final String PASS_CODE_NOT_VALID = "sec.pass_code_not_valid";
    public static final String INVALID_CHECK_CODE = "sec.invalid_token_check_code";
    public static final String USER_BLOCKED = "sec.user_blocked";
    public static final String COMMON_FORBIDDEN_ERROR = "sec.resource_forbidden";

}
