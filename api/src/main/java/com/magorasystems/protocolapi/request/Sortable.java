package com.magorasystems.protocolapi.request;

import java.util.List;

/**
 * Created by kemenov on 26.04.2017.
 */
public interface Sortable {

    String SORT_PARAMETER = "sort[]";

    List<String> getSorts();

}
