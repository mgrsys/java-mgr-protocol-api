package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Simple implementation of request.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class SimpleAuthRequest extends MetaAuthRequest<SimpleAuthMeta> {

    @JsonCreator
    public SimpleAuthRequest(@JsonProperty("login") String login,
                             @JsonProperty("password") String password,
                             @JsonProperty("meta") SimpleAuthMeta meta) {
        super(login, password, meta);
    }

    @Override
    public String toString() {
        return "SimpleAuthorizationRequest{} " + super.toString();
    }
}
