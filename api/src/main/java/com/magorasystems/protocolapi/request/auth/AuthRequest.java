package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Authorization request.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class AuthRequest {

    /* Login */
    @NotBlank
    private final String login;

    /* Password */
    @NotBlank
    private final String password;

    @JsonCreator
    public AuthRequest(@JsonProperty("login") final String login,
                       @JsonProperty("password") final String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "AuthorizationRequest{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
