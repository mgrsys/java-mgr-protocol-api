package com.magorasystems.protocolapi.request.auth;


/**
 * Created by kemenov on 22.12.2016.
 */
public final class Platform {

    public static final String IOS_PREFIX = "ios";
    public static final String ANDROID_PREFIX = "android";
    public static final String WEB_PREFIX = "web";


    public static String iOS(String version) {
        return platform(IOS_PREFIX, version);
    }

    public static String android(String version) {
        return platform(ANDROID_PREFIX, version);
    }

    public static String web(String version) {
        return platform(WEB_PREFIX, version);
    }

    public static boolean isAndroid(String platform) {
        return platform != null && platform.startsWith(ANDROID_PREFIX);
    }

    public static boolean isIOS(String platform) {
        return platform != null && platform.startsWith(IOS_PREFIX);
    }

    public static boolean isWeb(String platform) {
        return platform == null || platform.startsWith(WEB_PREFIX);
    }

    public static boolean isMobile(String platform) {
        return isAndroid(platform) || isIOS(platform);
    }

    private static String platform(String platform, String version) {
        if (version == null || version.isEmpty()) {
            return platform;
        }
        return platform + "." + version;
    }


}
