package com.magorasystems.protocolapi.request.auth;

/**
 * Authorization Meta data.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public interface AuthMeta {

    /* Gets device id */
    String getDeviceId();

    /* Gets version app */
    String getVersionApp();

    /* Gets push device id */
    String getPushDeviceId();
}
