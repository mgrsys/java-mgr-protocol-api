package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Simple implementation of meta.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 * Developed by Magora Team (magora-systems.com). 2015.
 */
public class SimpleAuthMeta extends AbstractAuthMeta {

    @JsonCreator
    public SimpleAuthMeta(@JsonProperty("deviceId") String deviceId,
                          @JsonProperty("versionApp") String versionApp,
                          @JsonProperty("pushDeviceId") String pushDeviceId) {
        super(deviceId, versionApp, pushDeviceId);
    }

    @Override
    public String toString() {
        return "SimpleAuthorizationMeta{" +
                "deviceId=" + getDeviceId() +
                ", pushDeviceId=" + getPushDeviceId() +
                ", versionApp=" + getVersionApp() +
                "}";
    }
}
