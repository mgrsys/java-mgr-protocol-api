package com.magorasystems.protocolapi.request.auth;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class AuthRequestHeaders {

    private AuthRequestHeaders() {
    }

    /* name of header that contains access token */
    public static final String ACCESS_TOKEN_HEADER = "X-ACCESS-TOKEN";

}
