package com.magorasystems.protocolapi.request.auth;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class AuthRequestParameters {

    private AuthRequestParameters() {
    }

    /* name of request parameter that contains access token */
    public static final String ACCESS_TOKEN_PARAMETER = "accessToken";

}
