package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Refresh token authorization request.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public class RefreshTokenRequest {

    /* refresh token */
    private final String refreshToken;

    @JsonCreator
    public RefreshTokenRequest(@JsonProperty("refreshToken") String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    @Override
    public String toString() {
        return "RefreshTokenRequest{" +
                "refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
