package com.magorasystems.protocolapi.request;

/**
 * Created by kemenov on 08.10.2016.
 */
public interface Cursorable {

    String PAGE_SIZE_PARAMETER = "pageSize";
    String CURSOR_PARAMETER = "cursor";

    int getPageSize();

    String getCursor();

}
