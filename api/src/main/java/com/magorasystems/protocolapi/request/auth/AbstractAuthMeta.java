package com.magorasystems.protocolapi.request.auth;

/**
 * Authorization implementation of meta data.
 *
 * @author Fyodor Kemenov
 * @author Valentin S. Bolkonsky.
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public abstract class AbstractAuthMeta implements AuthMeta {

    private final String deviceId;
    private final String versionApp;
    private final String pushDeviceId;

    protected AbstractAuthMeta(String deviceId, String versionApp, String pushDeviceId) {
        this.deviceId = deviceId;
        this.versionApp = versionApp;
        this.pushDeviceId = pushDeviceId;
    }

    @Override
    public String getDeviceId() {
        return deviceId;
    }

    @Override
    public String getVersionApp() {
        return versionApp;
    }

    @Override
    public String getPushDeviceId() {
        return pushDeviceId;
    }

}
