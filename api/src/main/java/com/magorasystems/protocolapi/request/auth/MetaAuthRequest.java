package com.magorasystems.protocolapi.request.auth;

/**
 * Meta auth info request implementation
 *
 * @author Valentin S. Bolkonsky.
 *         Proud to Code for Magora Systems/magora-systems.com/magora-systems.ru
 */
public class MetaAuthRequest<T extends AuthMeta> extends AuthRequest {

    /* meta data */
    private final T meta;

    public MetaAuthRequest(String login, String password, T meta) {
        super(login, password);
        this.meta = meta;
    }

    public T getMeta() {
        return meta;
    }

    @Override
    public String toString() {
        return "MetaAuthorizationRequest{" +
                "meta=" + meta +
                ", login=" + getLogin() +
                ", password=" + getPassword() +
                '}';
    }
}
