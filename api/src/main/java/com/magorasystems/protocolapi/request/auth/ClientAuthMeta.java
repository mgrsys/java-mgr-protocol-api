package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kemenov on 22.12.2016.
 */
public class ClientAuthMeta extends SimpleAuthMeta {

    private final String platform;

    @JsonCreator
    public ClientAuthMeta(@JsonProperty("platform") String platform,
                          @JsonProperty("deviceId") String deviceId,
                          @JsonProperty("versionApp") String versionApp,
                          @JsonProperty("pushDeviceId") String pushDeviceId) {

        super(deviceId, versionApp, pushDeviceId);
        this.platform = platform;

    }

    public String getPlatform() {
        return platform;
    }

    @Override
    public String toString() {
        return "AuthMeta{" +
                "deviceId=" + getDeviceId() +
                ", pushDeviceId=" + getPushDeviceId() +
                ", versionApp=" + getVersionApp() +
                "}";
    }


}
