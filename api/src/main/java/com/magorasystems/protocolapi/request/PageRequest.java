package com.magorasystems.protocolapi.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public class PageRequest implements Pageable {

    public static final int PAGE_SIZE = 100;

    @Min(0)
    protected int page = 0;

    @Min(1)
    @Max(1000)
    protected int pageSize = PAGE_SIZE;

    public PageRequest() {
    }

    public PageRequest(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    @Override
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PageRequest{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                '}';
    }
}
