package com.magorasystems.protocolapi.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public class CursorRequest implements Cursorable {

    public static final int PAGE_SIZE = 100;

    @Length(min = 1)
    protected String cursor;

    @Min(1)
    @Max(1000)
    protected int pageSize = PAGE_SIZE;

    public CursorRequest() {
    }

    public CursorRequest(String cursor, int pageSize) {
        this.cursor = cursor;
        this.pageSize = pageSize;
    }

    @Override
    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "CursorRequest{" +
                "cursor='" + cursor + '\'' +
                ", pageSize=" + pageSize +
                '}';
    }
}
