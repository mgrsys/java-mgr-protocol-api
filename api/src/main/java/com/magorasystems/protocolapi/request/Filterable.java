package com.magorasystems.protocolapi.request;

import java.util.List;

/**
 * Created by kemenov on 26.04.2017.
 */
public interface Filterable {

    String FILTER_PARAMETER = "filter[]";

    List<String> getFilters();

}
