package com.magorasystems.protocolapi.request.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kemenov on 22.12.2016.
 */
public class ClientAuthRequest extends MetaAuthRequest<ClientAuthMeta> {

    @JsonCreator
    public ClientAuthRequest(@JsonProperty("login") String login,
                             @JsonProperty("password") String password,
                             @JsonProperty("meta") ClientAuthMeta meta) {
        super(login, password, meta);
    }

    @Override
    public String toString() {
        return "AuthRequest{} " + super.toString();
    }
    
}