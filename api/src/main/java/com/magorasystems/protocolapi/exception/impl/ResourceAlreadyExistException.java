package com.magorasystems.protocolapi.exception.impl;

import com.magorasystems.protocolapi.exception.ResourceAlreadyExistTrouble;

import static com.magorasystems.protocolapi.response.ResponseCodes.ERROR_GENERIC_RESOURCE_ALREADY_EXISTS_CODE;

/**
 * Created by kemenov on 01.05.2017.
 */
public class ResourceAlreadyExistException extends AbstractBaseRuntimeException implements ResourceAlreadyExistTrouble {

    public ResourceAlreadyExistException(String field, String message) {
        super(ERROR_GENERIC_RESOURCE_ALREADY_EXISTS_CODE, field, message);

    }

    public ResourceAlreadyExistException() {
        this(null, "Resource already exists");
    }

}
