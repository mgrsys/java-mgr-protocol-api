package com.magorasystems.protocolapi.exception.impl;

import com.magorasystems.protocolapi.exception.CodeAwareTrouble;

/**
 * Created by kemenov on 01.05.2017.
 */
public abstract class AbstractBaseRuntimeException extends RuntimeException implements CodeAwareTrouble {

    private final String code;
    private final String field;

    protected AbstractBaseRuntimeException(String code, String field, String message) {
        super(message);
        this.code = code;
        this.field = field;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getField() {
        return field;
    }
}
