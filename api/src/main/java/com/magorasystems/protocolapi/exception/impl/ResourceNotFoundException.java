package com.magorasystems.protocolapi.exception.impl;

import com.magorasystems.protocolapi.exception.ResourceForbiddenTrouble;
import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;

/**
 * Developed by Magora Team (magora-systems.com). 2018.
 */
public class ResourceNotFoundException extends ResourceForbiddenException {

    public ResourceNotFoundException(String id) {
        super(id);
    }

    public ResourceNotFoundException(String field, String id) {
        super(field, id);
    }

}