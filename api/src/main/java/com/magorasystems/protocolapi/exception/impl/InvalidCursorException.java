package com.magorasystems.protocolapi.exception.impl;

import com.magorasystems.protocolapi.exception.impl.AbstractBaseRuntimeException;

import static com.magorasystems.protocolapi.response.ResponseCodes.ERROR_COMMON_CODE_INVALID_CURSOR;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public class InvalidCursorException extends AbstractBaseRuntimeException {

    public InvalidCursorException() {
        super(ERROR_COMMON_CODE_INVALID_CURSOR, "cursor", "Illegal cursor");
    }
}
