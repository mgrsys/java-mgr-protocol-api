package com.magorasystems.protocolapi.exception;

/**
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface CodeAwareTrouble {

    String getCode();
    String getMessage();
    String getField();

}
