package com.magorasystems.protocolapi.exception.impl;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2017.
 */
public class CodeAwareRuntimeException extends AbstractBaseRuntimeException {

    protected CodeAwareRuntimeException(String code, String message) {
        super(code, null, message);
    }

    protected CodeAwareRuntimeException(String code, String field, String message) {
        super(code, field, message);
    }
}