package com.magorasystems.protocolapi.exception.impl;

import com.magorasystems.protocolapi.exception.ResourceForbiddenTrouble;
import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;

/**
 * Developed by Magora Team (magora-systems.com). 2017.
 */
public class ResourceForbiddenException extends RuntimeException implements ResourceForbiddenTrouble {

    private final String field;

    public ResourceForbiddenException(String id) {
        super("Resource forbidden id = " + id);
        this.field = null;
    }

    public ResourceForbiddenException(String field, String id) {
        super("Resource forbidden id = " + id);
        this.field = field;
    }

    @Override
    public String getCode() {
        return AuthResponseCodes.COMMON_FORBIDDEN_ERROR;
    }

    @Override
    public String getField() {
        return field;
    }
}